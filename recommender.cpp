#include <iostream>
#include <fstream>
#include <cstdlib>
#include <gsl/gsl_rng.h>
#include <boost/unordered_map.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/multi_array.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <vector>
#include <iomanip>
#include <string>
#include <sys/time.h>


#include "Node.h"
#include "Link.h"
#include "Group.h"
#include "utils.h"

#define LOGSIZE 5000
#define BOOST_DISABLE_ASSERTS

typedef boost::unordered_map<int, Node> node_map;
typedef boost::unordered_map<std::string, int> IdMap;
typedef std::vector<int> IVector;
typedef std::vector< boost::tuple<float, Node*> > SimilV;
typedef std::vector< boost::tuple<Node*, Node*> > tuple_list;
typedef std::vector<Node*> node_list;

int num_movements = 0;
int tried_num_movements = 0;
int num_total_groups1 = 0;
int num_total_groups2 = 0;


/*!
 * Calculate the similarity between all Node pairs in the same partition according to their Links to the other partition.
 * Similarities of one Node with the others are stored in SimilV ordered.
 * @param nl Node list containing all the Nodes within a partition. 
 * @K Number of values a Link can be weighted.
 */
void getSimilarities(node_list *nl, int num_marks) {

    Node *n1, *n2;
    double similarity;
    bool inserted;

    for (node_list::iterator it1 = (*nl).begin(); it1 != (*nl).end(); it1++){
        n1 = (*it1);
        for (node_list::iterator it2 = (*nl).begin(); it2 != (*nl).end(); it2++){
            n2 = (*it2);

            /* similarity = dotProduct (n1, n2); */
            similarity = 1 - meanDistance (n1, n2, num_marks);
            inserted = false;
            for (SimilV::iterator sort_it = n1->similarityv.begin(); sort_it != n1->similarityv.end(); sort_it++)
                if (sort_it->get<0>() < similarity){
                    n1->similarityv.insert(sort_it, boost::tuple<float, Node*>(similarity, n2));
                    inserted = true;
                    break;
                }
            if (!inserted) n1->similarityv.push_back(boost::tuple<float, Node*>(similarity, n2));
        } 
    } 

    /* for (SimilV::iterator it1 = n1->similarityv.begin(); it1 != n1->similarityv.end(); it1++) */
    /*     std::cout << n1->getRealId() << " " << it1->get<1>()->getRealId() << ": " <<  it1->get<0>() << "\n"; */
}



/*!
 * Calculate H between two Node groups. 
 * @param K Number of values a Link can be weighted.
 * @param g2glinks Pointer within the g2glinks matrix pointing to the specific values between this two Groups.
 * @return H between the two Groups.
 */
double group2GroupH (int K, int *g2glinks) {

	double H = 0.0;

	H += gsl_sf_lnfact(*g2glinks + K - 1);
	for (int i = 1; i < K+1; ++i) {
        H -= gsl_sf_lnfact(*(g2glinks + i));
	}
    return H;
}


/*
 * Calculate the H of all system at the current state. Consists on calculating the H between all the Group pairs (see group2groupH()). Used to get initial H of the system.
 * @param K Number of values a Link can be weighted.
 * @param g1 Set of Groups from the first partition.
 * @param g2 Set of Groups from the second partition.
 * @param d1_size Number of Nodes of the first partition.
 * @param d2_size Number of Nodes of the second partition.
 * @param g2glinks Matrix where all the Link count between all group pairs is stored.
 * @param ng1 Number of Groups of the first partition.
 * @param ng2 Number of Groups of the second partition.
 * @return Total H of the system.
 */
double hkState (int K, Groups *g1, Groups *g2, int d1_size, int d2_size, int *g2glinks, int ng1, int ng2, int ng2_total) {

	double H = 0.0;

	for (Groups::iterator it1 = g1->begin(); it1 != g1->end(); ++it1)
		for (Groups::iterator it2 = g2->begin(); it2 != g2->end(); ++it2)
			H += group2GroupH(K, g2glinks+(it1->second.getId()*ng2_total*(K+1) + it2->second.getId()*(K+1)));
		
	H -= gsl_sf_lnfact(d1_size - ng1) + gsl_sf_lnfact(d2_size - ng2);
	
	return H;
}


/*!
 * Create Groups of Nodes from the given partition. If nodespergroup is bigger than 1, Nodes are grouped according to their similarity.
 * @param nl Node list containing the Nodes from a partition.
 * @param groups Group array where the created Groups are stored.
 * @param keys List of Node ids used to control a node is only assigned to a group once.
 * @param K Number of values a Link can be weighted.
 * @param nodespergroup Number of Nodes assigned to a group.
 * @param random Indicates if Group assignment is random or sequential.
 */
void createGroupsPartition (node_list *nl, Groups *groups, IVector keys, int K, int nodespergroup, bool random, int mark) {
    int groupid=0, simil_count=0, i=0;
    Node *n, *sim;

    if (random){
        for (node_list::iterator itn = nl->begin(); itn != nl->end(); itn++) {
            groupid = i;
            (*itn)->setGroup(groupid);
            if ( (*groups).find(groupid) == (*groups).end() )
                (*groups)[groupid] = Group(groupid, K);
            (*groups)[groupid].members[(*itn)->getId()] = *itn;
            simil_count++;
            if (simil_count==nodespergroup) {
                i++; simil_count=0;
            }
            
        }

    }else{
        getSimilarities(nl, mark);
        for (unsigned int k_ind = 0; k_ind<keys.size(); k_ind++) {
            n = (*nl)[keys[k_ind]];
            n->setGroup(groupid);
            (*groups)[groupid] = Group(groupid, K);
            (*groups)[groupid].members[n->getId()] = n;
            keys.erase(keys.begin()+k_ind); k_ind--;

            simil_count = i = 0;
            while (simil_count < nodespergroup-1 && keys.size()>0){
                sim = n->similarityv[i].get<1>();

                for (unsigned int j=0; j<keys.size(); j++){
                    if (keys[j] == sim->getId() && sim->getId() != n->getId()){
                        keys.erase(keys.begin()+j);
                        sim->setGroup(groupid);
                        (*groups)[groupid].members[sim->getId()] = sim;
                        simil_count++;
                        break;
                    }
                }
                i++;
            }

            groupid++;
        }
    }
}


/*!
 * Fill the g2glinks matrix.
 * @param g1 Set of Groups from the first partition.
 * @param g2 Set of Groups from the second partition.
 * @param K Number of values a Link can be weighted.
 * @param g2glinks Matrix where all the Link count between all Group pairs is stored.
 * @param ng1 Number of Groups of the first partition.
 * @param ng2 Number of Groups of the second partition.
 */
void initG2GLinks (Groups *groups1, Groups *groups2, int K, int *g2glinks, int ng1, int ng2) {
    GroupNodes::iterator it1, it2;
    Groups::iterator g1, g2;
    int nweight[K];
    int *pg2glinks;
    Links::iterator nit;
    int ngrouplinks;

    for (g1 = groups1->begin(); g1 != groups1->end(); g1++) {
        for (g2 = groups2->begin(); g2 != groups2->end(); g2++) {
            memset(nweight, 0, sizeof(int) * K);
            ngrouplinks = 0;

            //Groups 1 info filling

            for (it1 = g1->second.members.begin(); it1 != g1->second.members.end(); it1++)
                for (nit = it1->second->neighbours.begin(); nit != it1->second->neighbours.end(); nit++){
                    for (it2 = g2->second.members.begin(); it2 != g2->second.members.end(); it2++)
                        if (nit->second.getNode()->getId() == it2->second->getId()){
                            ++ngrouplinks;
                            nweight[nit->second.getWeight()]++;
                        }
                }

            pg2glinks = &g2glinks[ g1->second.getId()*ng2*(K+1) + g2->second.getId()*(K+1) ];
            *pg2glinks = ngrouplinks;
            for (int i=1; i<K+1; ++i) {
                *(pg2glinks + i) = nweight[i-1];
            }
        }
    }
}



/*!
 * Perform a Monte Carlo step in our system. It consists on moving all the nodes from one or another partition (randomly chosen) decorrelationStep times between groups of the 
 * same partition. Each time we calculate dH and if its valid (we want to minimize H), we update H value and continue with next step. If not, we undo the movement and continue with
 * the next step. 
 * @param g1 Set of Groups from the first partition.
 * @param g2 Set of Groups from the second partition.
 * @param d1 Set of Nodes from the first partition.
 * @param d2 Set of Nodes from the second partition.
 * @param rgen Random generator.
 * @param H Energy of the system.
 * @param lnfactlist Static list of factorial logarithm of size logsize.
 * @param logsize Size of lnfactlist.
 * @param nnod1 Number of Nodes in first partition (d1).
 * @param nnod2 Number of Nodes in second partition (d2).
 * @param g2glinks Matrix where all the link count between all Group pairs is stored.
 * @param keys1 List of Node ids from the first partition.
 * @param keys2 List of Node ids from the second partition.
 * @param ng1 Number of (non empty) Groups from the first partition (g1).
 * @param ng2 Number of (non empty) Groups from the second partition (g2).
 * @param ng1_total Total number of Groups from the first partition.
 * @param ng2_total Total number of Groups from the second partition.
 */
int mcStepKState(Groups *g1, Groups *g2, node_list *d1, node_list *d2, gsl_rng *rgen, double *H, int K, double *lnfactlist, int logsize, int nnod1, int nnod2, int *g2glinks, int decorStep, IVector *keys1, IVector *keys2, int *ng1, int *ng2, int ng1_total, int ng2_total){


    /* struct timeval stop, start; */
    Groups *g;
    node_list *d_move, *d_nomove;
    int *t1_g2glinks, *t2_g2glinks;
    int factor;
	Group *src_g, *dest_g;
	int newgrp, oldgrp, dice, set_size_move, id;
    bool set_ind;
	Node *n;
	double dH;
    int *ng;
    int ng_total;
    IVector *keys;
    int K1 = K+1;

    int bnnod = (ng1_total>ng2_total) ? ng1_total+1 : ng2_total+1;
    double set_ratio = (double)(nnod1*nnod1-1) / (double)(nnod1*nnod1+nnod2*nnod2-2);

	bool visitedgroup[bnnod]; 


    for (int i=0; i < bnnod; i++)
        visitedgroup[i] = false;

    factor = (nnod1+nnod2)*decorStep;
    for (int move=0; move<factor; move++) {
        /* gettimeofday(&start, NULL); */
        dH = 0.0;
        if (gsl_rng_uniform(rgen) < set_ratio){
            g = g1;d_move = d1;d_nomove = d2;set_ind = true;keys = keys1; ng = ng1; ng_total = ng1_total;
        }else{
            g = g2;d_move = d2;d_nomove = d1;set_ind = false;keys = keys2; ng = ng2; ng_total = ng2_total;
        }
        set_size_move = ng_total;


        dice = gsl_rng_uniform(rgen) * set_size_move;
        n = (*d_move)[(*keys)[dice]];
        oldgrp = n->getGroup();
        newgrp = gsl_rng_uniform(rgen) * set_size_move;
        while ( newgrp == oldgrp ) 
            newgrp = gsl_rng_uniform(rgen) * set_size_move;

        src_g = &(*g)[oldgrp];
        dest_g = &(*g)[newgrp];

        for (Links::iterator it = n->neighbours.begin(); it != n->neighbours.end(); ++it){
            id = (*(*d_nomove)[it->second.getNode()->getId()]).getGroup();
            if (!visitedgroup[id]){
                if (set_ind){
                    t1_g2glinks = g2glinks + src_g->getId()*ng2_total*K1 + id*K1;
                    t2_g2glinks = g2glinks + dest_g->getId()*ng2_total*K1 + id*K1;
                    dH -= lnfactlist[*t1_g2glinks + K - 1];
                    dH -= lnfactlist[*t2_g2glinks + K - 1];
                }else{
                    t1_g2glinks = g2glinks + id*ng2_total*K1 + src_g->getId()*K1;
                    t2_g2glinks = g2glinks + id*ng2_total*K1 + dest_g->getId()*K1;
                    dH -= lnfactlist[*t1_g2glinks + K - 1];
                    dH -= lnfactlist[*t2_g2glinks + K - 1];
                }

                for (int i = 1; i<K+1; ++i){
                    dH -= -lnfactlist[*(t1_g2glinks + i)];
                    dH -= -lnfactlist[*(t2_g2glinks + i)];
                }
                visitedgroup[id] = true;
            }
        }

        if ( src_g->members.size() == 1 || dest_g->members.size() == 0)
            dH += lnfactlist[set_size_move - *ng];

        if (set_ind){
            src_g->removeNodeS1(n, g2glinks, ng2_total, K1);
            dest_g->addNodeS1(n, g2glinks, ng2_total, K1);
        }else{
            src_g->removeNodeS2(n, g2glinks, ng2_total, K1);
            dest_g->addNodeS2(n, g2glinks, ng2_total, K1);
        }

        if (src_g->members.size() == 0) *ng -= 1;
        if (dest_g->members.size() == 1) *ng += 1;

        for (Links::iterator it = n->neighbours.begin(); it != n->neighbours.end(); ++it){
            id = (*(*d_nomove)[it->second.getNode()->getId()]).getGroup();
            if (visitedgroup[id]){
                if (set_ind){
                    t1_g2glinks = g2glinks + src_g->getId()*ng2_total*K1 + id*K1;
                    t2_g2glinks = g2glinks + dest_g->getId()*ng2_total*K1 + id*K1;
                    dH += lnfactlist[*t1_g2glinks + K - 1];
                    dH += lnfactlist[*t2_g2glinks + K - 1];
                }else{
                    t1_g2glinks = g2glinks + id*ng2_total*K1 + src_g->getId()*K1;
                    t2_g2glinks = g2glinks + id*ng2_total*K1 + dest_g->getId()*K1;
                    dH += lnfactlist[*t1_g2glinks + K - 1];
                    dH += lnfactlist[*t2_g2glinks + K - 1];
                }

                for (int i = 1; i<K+1; ++i){
                    dH += -lnfactlist[*(t1_g2glinks + i)];
                    dH += -lnfactlist[*(t2_g2glinks + i)];
                }

                visitedgroup[id] = false;
            }
        }	

        if ( src_g->members.size() == 0 || dest_g->members.size() == 1)
            dH -= lnfactlist[set_size_move - *ng];

        
        if ( dH <= 0.0 || gsl_rng_uniform(rgen) < exp(-dH)){
            num_movements++;
            *H += dH;
        }else{
            tried_num_movements++;
            if(set_ind){
                dest_g->removeNodeS1(n, g2glinks, ng2_total, K1);
                src_g->addNodeS1(n, g2glinks, ng2_total, K1);
            }else{
                dest_g->removeNodeS2(n, g2glinks, ng2_total, K1);
                src_g->addNodeS2(n, g2glinks, ng2_total, K1);
            }
            if (src_g->members.size() == 1) *ng += 1;
            if (dest_g->members.size() == 0) *ng -= 1;
        }
        /* gettimeofday(&stop, NULL); */
        /* printf("Time %lu\n", stop.tv_usec - start.tv_usec); */
    } //End of MC step

	return 0;
}


/*!
 * Equilibrate the system before we start the sampling.
 * @param g1 Set of Groups from the first partition.
 * @param g2 Set of Groups from the second partition.
 * @param d1 Set of Nodes from the first partition.
 * @param d2 Set of Nodes from the second partition.
 * @param rgen Random generator.
 * @param H Energy of the system.
 * @param lnfactlist Static list of factorial logarithm of size logsize.
 * @param logsize Size of lnfactlist.
 * @param nnod1 Number of Nodes in first partition (d1).
 * @param nnod2 Number of Nodes in second partition (d2).
 * @param g2glinks Matrix where all the Link count between all Group pairs is stored.
 * @param keys1 List of Node ids from the first partition.
 * @param keys2 List of Node ids from the second partition.
 * @param ng1 Number of (non empty) Groups from the first partition (g1).
 * @param ng2 Number of (non empty) Groups from the second partition (g2).
 * @param ng1_total Total number of Groups from the first partition.
 * @param ng2_total Total number of Groups from the second partition.
 */
void thermalizeMCKState(Groups *g1, Groups *g2, node_list *d1, node_list *d2, gsl_rng *rgen, double *H, int K, double *lnfactlist, int logsize, int nnod1, int nnod2, int *g2glinks, int decorStep, IVector *keys1, IVector *keys2, int *ng1, int *ng2, int ng1_total, int ng2_total){

    double HMean0=1.e10, HStd0=1.e-10, HMean1, HStd1, *Hvalues;
    int nrep=20;
    int equilibrated=0;

    Hvalues = (double*)calloc(nrep, sizeof(double));

    do {
        for (int i=0; i<nrep; i++){
            mcStepKState(g1, g2, d1, d2, rgen, H, K, lnfactlist, logsize, nnod1, nnod2, g2glinks, decorStep, keys1, keys2, ng1, ng2, ng1_total, ng2_total);
            Hvalues[i] = *H;
        }

        HMean1 = mean(Hvalues, nrep);
        HStd1 = stddev(Hvalues, nrep);

        if (HMean0 - HStd0 / sqrt(nrep) < HMean1 + HStd1 / sqrt(nrep)) {
            equilibrated++;
            fprintf(stderr, "#\tequilibrated (%d/5) H=%lf\n", equilibrated, HMean1);

        } else {
            fprintf(stderr, "#\tnot equilibrated yet H0=%g+-%g H1=%g+-%g\n", HMean0, HStd0 / sqrt(nrep), HMean1, HStd1 / sqrt(nrep));
            HMean0 = HMean1;
            HStd0 = HStd1;
            equilibrated = 0;
        }
    } while (equilibrated < 5);

    return;
}


/*!
 * Look for how many movements we have to do before dH is significative.
 * @param g1 Set of Groups from the first partition.
 * @param g2 Set of Groups from the second partition.
 * @param d1 Set of Nodes from the first partition.
 * @param d2 Set of Nodes from the second partition.
 * @param rgen Random generator.
 * @param H Energy of the system.
 * @param lnfactlist Static list of factorial logarithm of size logsize.
 * @param logsize Size of lnfactlist.
 * @param nnod1 Number of Nodes in first partition (d1).
 * @param nnod2 Number of Nodes in second partition (d2).
 * @param g2glinks Matrix where all the Link count between all Group pairs is stored.
 * @param keys1 List of Node ids from the first partition.
 * @param keys2 List of Node ids from the second partition.
 * @param ng1 Number of (non empty) Groups from the first partition (g1).
 * @param ng2 Number of (non empty) Groups from the second partition (g2).
 * @param ng1_total Total number of Groups from the first partition.
 * @param ng2_total Total number of Groups from the second partition.
 * @return decorrelation step.
 */
int getDecorrelationKState(Groups *g1, Groups *g2, node_list *d1, node_list *d2, gsl_rng *rgen, double *H, int K, double *lnfactlist, int logsize, int nnod1, int nnod2, int *g2glinks, IVector *keys1, IVector *keys2, int *ng1, int *ng2, int ng1_total, int ng2_total){

    int nrep = 10, step, norm = 0;
    int x1, x2, i;
    double *decay, *decay1, *decay2;
    double meanDecay, meanDecay1, meanDecay2, sigmaDecay;
    double y11 = 0.0, y12 = 0.0, y21 = 0.0, y22 = 0.0, result;
	Groups g2t, g1t;

    decay1 = (double*) calloc(nrep, sizeof(double));
    decay2 = (double*) calloc(nrep, sizeof(double));

    x2 = (nnod1 + nnod2) / 5;
    if (x2 < 10)
        x2 = 10;
    x1 = x2 / 4;

    for (i=0; i<nrep; i++) {

        fprintf(stderr, "Estimating decorrelation time %d/%d\n", i+1, nrep);
        g1t = (*g1);
        g2t = (*g2);

        for (step=0; step<=x2; step++) {
            mcStepKState(g1, g2, d1, d2, rgen, H, K, lnfactlist, logsize, nnod1, nnod2, g2glinks, 1, keys1, keys2, ng1, ng2, ng1_total, ng2_total);

            if (step == x1){
                y11 = mutualInfo(g1, &g1t, nnod1, nnod2);
                y12 = mutualInfo(g2, &g2t, nnod1, nnod2);
            }
        }
        y21 = mutualInfo(g1, &g1t, nnod1, nnod2);
        y22 = mutualInfo(g2, &g2t, nnod1, nnod2);

        (nnod1>1) ? decay1[i] = 2 * getDecay(nnod1, x1, x2, y11, y21) : decay1[i] = 1.e-6;
        (nnod2>1) ? decay2[i] = 2 * getDecay(nnod2, x1, x2, y12, y22) : decay2[i] = 1.e-6;

        fprintf(stderr, "# Decorrelation times (estimate %d) = %g %g\n\n", i + 1, decay1[i], decay2[i]);

        if (decay1[i] < 0. || decay2[i] < 0.){
            i--;
            fprintf(stderr, "\t# ignoring...\n");
        }
    }

    meanDecay1 = mean(decay1, nrep);
    meanDecay2 = mean(decay2, nrep);

    if (meanDecay1 > meanDecay2){
        meanDecay = meanDecay1;
        decay = decay1;
        sigmaDecay = stddev(decay1, nrep);
    }else{
        meanDecay = meanDecay2;
        decay = decay2;
        sigmaDecay = stddev(decay2, nrep);
    }

    result = meanDecay*nrep;

    for (i=0; i<nrep; i++)
       (fabs(decay[i] - meanDecay) / sigmaDecay > 2) ? result -= decay[i] : ++norm;

    return (int)(result / norm + 0.5);
}




/* ##################################### */
int main(int argc, char **argv){

    int decorStep;
	int weight, logsize=LOGSIZE;
    std::string id1, id2;
	double H;
    double *lnfactlist;
    tuple_list queries;
    node_map nm1, nm2;
	IdMap rd1, rd2;
    double *scores;
    double *pscores;
    int *g2glinks;
    int *tg2glinks;
    int ng1_total, ng2_total;
	Groups groups2, groups1;
	gsl_rng *randomizer;
    char* outFile = new char[100];
	char* tFileName;
	char* qFileName;
    Node node1, node2;
	int stepseed, nodespergroup = 1, iterations = 10000;
	int mark, k, i, j, n, nk, tmpid1 = 0, tmpid2 = 0, tid1, tid2;
    int nnod1, nnod2, nqueries = 0;
    int ng1 = 0, ng2 = 0;
    time_t start;
    time_t end;
    /* double TH; */
    std::ofstream outfile;

	parseArguments(argc, argv, &tFileName, &qFileName, &stepseed, &mark, &iterations, &nodespergroup, &outFile);

	randomizer = gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(randomizer, stepseed);

    fprintf(stderr, "Reading input files and creating groups...\n");
    start = time(NULL);
	std::ifstream tFile(tFileName);
	if (tFile.is_open()){
		while (tFile >> id1 >> id2 >> weight){
            if (rd1.find(id1) == rd1.end()){
                rd1.insert(IdMap::value_type(id1,tmpid1));
                tmpid1++;
            }
            if (rd2.find(id2) == rd2.end()){
                rd2.insert(IdMap::value_type(id2,tmpid2));
                tmpid2++;
            }
            tid1 = rd1[id1]; 
            tid2 = rd2[id2];
            nm1.insert(node_map::value_type(tid1, Node(id1, tid1)));
            nm2.insert(node_map::value_type(tid2, Node(id2, tid2)));
            nm1[tid1].neighbours.insert(Links::value_type(tid2,Link(&nm2[tid2], weight)));
			nm2[tid2].neighbours.insert(Links::value_type(tid1,Link(&nm1[tid1], weight)));
            
		}
		tFile.close();
	}else
		std::cout << "Couldn't open file " << tFileName << "\n";

	std::ifstream qFile(qFileName);
	if (qFile.is_open()){
		while (qFile >> id1 >> id2){
            if (rd1.find(id1) == rd1.end()){
                rd1.insert(IdMap::value_type(id1,tmpid1));
                tid1 = rd1[id1]; 
                nm1.insert(node_map::value_type(tid1, Node(id1, tid1)));
                tmpid1++;
            }

            if (rd2.find(id2) == rd2.end()){
                rd2.insert(IdMap::value_type(id2,tmpid2));
                tid2 = rd2[id2];
                nm2.insert(node_map::value_type(tid2, Node(id2, tid2)));
                tmpid2++;
            }

            tid1 = rd1[id1]; 
            tid2 = rd2[id2];
            nm1[tid1].neighbours.erase(tid2);
            nm2[tid2].neighbours.erase(tid1);
            queries.push_back(boost::tuple<Node*, Node*>(&(nm1[tid1]), &(nm2[tid2])));
		}
		qFile.close();
	}else
		std::cout << "Couldn't open file " << tFileName << "\n";

    node_list d1 (nm1.size());
    node_list d2 (nm2.size());

    i = 0;
    for (node_map::iterator it = nm1.begin(); it != nm1.end(); it++){
        d1[i] = &(it->second);++i;
    }

    i = 0;
    for (node_map::iterator it = nm2.begin(); it != nm2.end(); it++){
        d2[i] = &(it->second);++i;
    }


    lnfactlist = genLogFactList(logsize);

    nnod1 = d1.size();
    nnod2 = d2.size();
    nqueries = queries.size();

    IVector keys1(nnod1), keys2(nnod2);
    i = 0;
    for(node_list::iterator it = d1.begin(); it != d1.end(); ++it) {
        keys1[i] = (*it)->getId();
        ++i;
    }
    i = 0;
    for(node_list::iterator it = d2.begin(); it != d2.end(); ++it) {
        keys2[i] = (*it)->getId();
        ++i;
    }

    scores = new double[nqueries*mark]();
    pscores = scores;

    /* exit(0); */

    createGroupsPartition (&d1, &groups1, keys1, mark, nodespergroup, true, mark);
        /* printf("############GROUPS1################\n"); */
        /* printGroups(groups1, mark); */
    createGroupsPartition (&d2, &groups2, keys2, mark, nodespergroup, true, mark);
        /* printf("############GROUPS2################\n"); */
        /* printGroups(groups2, mark); */
    ng1_total = ng1 = groups1.size();
    ng2_total = ng2 = groups2.size();

    g2glinks = new int[ng1_total*ng2_total*(mark+1)]();
    initG2GLinks (&groups1, &groups2, mark, g2glinks, ng1_total, ng2_total);
    end = time(NULL);
    fprintf(stderr, "Number of groups: %d %d | Time Spent: %lu secs\n\n", ng1, ng2, end-start);
    
    /* for(i=0;i<nnod1*nnod2*(mark+1);i++) */
    /*     printf("%d\n",*(g2glinks+i)); */


    /****** HKSTATE *****/
    fprintf(stderr, "Calculating initial H...\n");
    start = time(NULL);
	H = hkState(mark, &groups1, &groups2, nnod1, nnod2, g2glinks, ng1, ng2, ng2_total);
    end = time(NULL);
    fprintf(stderr, "Initial H: %f | Time Spent: %lu secs\n\n", H, end-start);

    /****** DECORRELATION *****/
    fprintf(stderr, "Calculating decorrelation step...\n");
    start = time(NULL);
    decorStep = getDecorrelationKState(&groups1, &groups2, &d1, &d2, randomizer, &H, mark, lnfactlist, logsize, nnod1, nnod2, g2glinks, &keys1, &keys2, &ng1, &ng2, ng1_total, ng2_total);
    /* decorStep = 1; */
    if (decorStep == 0) decorStep = 1;
    end = time(NULL);
    fprintf(stderr, "Decorrelation step: %d | Time Spent: %lu secs\n\n", decorStep, end-start);

    /****** THERMALIZATION *****/
    fprintf(stderr, "Thermalizing...\n");
    start = time(NULL);
    thermalizeMCKState(&groups1, &groups2, &d1, &d2, randomizer, &H, mark, lnfactlist, logsize, nnod1, nnod2, g2glinks, decorStep, &keys1, &keys2, &ng1, &ng2, ng1_total, ng2_total);
    end = time(NULL);
    fprintf(stderr, "Non-empty groups: %d %d | Time Spent: %lu secs\n\n", ng1, ng2, end-start);


    /***** MONTECARLO *****/
    fprintf(stderr, "Starting MC Steps...\n");
    start = time(NULL);
    int indquery;
	for(i=0; i<iterations; i++){
		mcStepKState(&groups1, &groups2, &d1, &d2, randomizer, &H, mark, lnfactlist, logsize, nnod1, nnod2, g2glinks, decorStep, &keys1, &keys2, &ng1, &ng2, ng1_total, ng2_total);
        num_total_groups1 += ng1;
        num_total_groups2 += ng2;
        /* TH = hkState(mark, &groups1, &groups2, nnod1, nnod2, &gglinks, ng1, ng2, ng2_total); */
        /* std::cout << std::setprecision(20) << H << "    " << TH << "\n\n"; */
        /* std::cout << std::setprecision(20) << i << " " << H << "\n"; */
        /* printf("############GROUPS1################\n"); */
        /* printGroups(groups1, mark); */
        /* printf("############GROUPS2################\n"); */
        /* printGroups(groups2, mark); */
        /* printf("###################################\n"); */
        indquery = 0;
        pscores = scores;
        for (tuple_list::iterator it = queries.begin(); it != queries.end(); ++it) {
            tg2glinks = g2glinks + (*it->get<0>()).getGroup()*ng2_total*(mark+1) + (*it->get<1>()).getGroup()*(mark+1);
            n = *tg2glinks;
            for (k=1; k<mark+1; k++) {
                nk = *(tg2glinks + k);
                *pscores++ += (float)(nk + 1) / (float)(n + mark);
            }
            indquery++;
        }


        if (i % 100 == 0) { 
            outfile.open(outFile);
            indquery = 0;
            pscores = scores;
            for (tuple_list::iterator it = queries.begin(); it != queries.end(); ++it) {
                outfile << (*it->get<0>()).getRealId() <<' '<< (*it->get<1>()).getRealId() << " ";
                for (k=0; k<mark; k++){
                    outfile << std::setprecision(6) << *pscores++/(double)iterations << " ";
                }
                indquery++;
                outfile << " " << ng1 << " " << ng2 << "\n";
            }
            outfile << "Mean used groups: " << (float)num_total_groups1/(float)i << " " << (float)num_total_groups2/(float)i << "\n";
            outfile << "Rejected movements: " << tried_num_movements << "/" << tried_num_movements+num_movements << "\n";
            outfile.close();
        }
	}
    end = time(NULL);
    fprintf(stderr, "Done. Time Spent: %lu secs\n\n", end-start);

    pscores = scores;
    for (j=0; j<nqueries; j++)
        for (k=0; k<mark; k++){
            *pscores++ /= (double)iterations;
        }

    outfile.open(outFile);
    indquery = 0;
    pscores = scores;
    for (tuple_list::iterator it = queries.begin(); it != queries.end(); ++it) {
        outfile << (*it->get<0>()).getRealId() <<' '<< (*it->get<1>()).getRealId() << " ";
        std::cout << (*it->get<0>()).getRealId() <<' '<< (*it->get<1>()).getRealId() << " ";
        for (k=0; k<mark; k++){
            outfile << std::setprecision(6) << *pscores << " ";
            std::cout << std::setprecision(6) << *pscores++ << " ";
        }
        indquery++;
        std::cout << "\n";
    }
    outfile << "Mean used groups: " << (float)num_total_groups1/(float)iterations << " " << (float)num_total_groups2/(float)iterations << "\n";
    std::cout << "Mean used groups: " << (float)num_total_groups1/(float)iterations << " " << (float)num_total_groups2/(float)iterations << "\n";
    outfile << "Rejected movements: " << tried_num_movements << "/" << tried_num_movements+num_movements << "\n";
    std::cout << "Rejected movements: " << tried_num_movements << "/" << tried_num_movements+num_movements << "\n";

    free(lnfactlist);
	gsl_rng_free(randomizer);
	return 0;

}


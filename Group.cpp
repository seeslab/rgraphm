#include "Group.h"
#include <string.h>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <cstdlib>


Group::Group( int id ) {

	Group::id = id;
}

Group::Group ( int id, int K ) {
		
	Group::id = id;
	Group::k = K;

}

Group::Group() {}


int Group::getId () const {

	return Group::id;
}


void Group::setId ( int id ) {

	Group::id = id;
}

int Group::addNodeS1 ( Node *n, int *g2glinks, int ng2, int K ) {

    int *tg2glinks;
    int gid;
	n->setGroup(Group::id);
	Group::members[n->getId()] = n;
	for(Links::iterator it = n->neighbours.begin(); it != n->neighbours.end(); ++it){
        gid = it->second.getNode()->getGroup();
        tg2glinks = g2glinks + Group::id*ng2*K + gid*K;
        ++(*tg2glinks);
        ++(*(tg2glinks + it->second.getWeight()+1));
	}
 	
	return 0;
}

int Group::addNodeS2 ( Node *n, int *g2glinks, int ng2, int K ) {

    int *tg2glinks;
    int gid;
	n->setGroup(Group::id);
	Group::members[n->getId()] = n;
	for(Links::iterator it = n->neighbours.begin(); it != n->neighbours.end(); ++it){
        gid = it->second.getNode()->getGroup();
        tg2glinks = g2glinks + gid*ng2*K + Group::id*K;
        ++(*tg2glinks);
        ++(*(tg2glinks + it->second.getWeight()+1));
	}
	return 0;
}


int Group::removeNodeS1 ( Node *n, int *g2glinks, int ng2, int K ) {

    int *tg2glinks;
    int gid;
	Group::members.erase(n->getId());
	for(Links::iterator it = n->neighbours.begin(); it != n->neighbours.end(); ++it){
        gid = it->second.getNode()->getGroup();
        tg2glinks = g2glinks + Group::id*ng2*K + gid*K;
        --(*tg2glinks);
        --(*(tg2glinks + it->second.getWeight()+1));
	}
	return 0;
}

int Group::removeNodeS2 ( Node *n, int *g2glinks, int ng2, int K ) {

    int *tg2glinks;
    int gid;
	Group::members.erase(n->getId());
	for(Links::iterator it = n->neighbours.begin(); it != n->neighbours.end(); ++it){
        gid = it->second.getNode()->getGroup();
        tg2glinks = g2glinks + gid*ng2*K + Group::id*K;
        --(*tg2glinks);
        --(*(tg2glinks + it->second.getWeight()+1));
	}
	return 0;
}

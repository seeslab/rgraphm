#ifndef LINK_H
#define LINK_H

struct Node;

class Link {
	Node* n;
	int weight;

	public:
		Link();
		Link(Node* n, int weight);

		Node* getNode() const;
		int getWeight() const;

		void setNode(Node* n);
		void setWeight(int weigth);
};

#endif
